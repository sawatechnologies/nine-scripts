

const axios = require('axios');
const nineToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMDBjNWQxN2RjNDYwMzE2YWRlY2Y5YyIsImVtYWlsIjoiYW1tYWRAbWVldG5pbmUuY29tIiwidHlwZSI6ImFkbWluLWFjY2VzcyIsImlhdCI6MTY0NjM4NTA3MSwiZXhwIjoxNjYxOTM3MDcxLCJpc3MiOiJ3d3cubWVldG5pbmUuY28ifQ.hPJoUra79BNH6IbO1MsJ2y9LtKw1a_CVilCuoOvOank";
const ObjectsToCsv = require('objects-to-csv');

getUserById = async (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`https://api.meetnine.cn/api/dashboard/users?id=${id}`, {
            headers: {
                "x-access-token": nineToken
            }
        })
            .then(res => {
                let user = (({ id, nickName, albumPictures, realPicture, registrationStatus }) => ({ id, nickName, albumPictures, realPicture, registrationStatus }))(res.data.data)
                resolve({
                    ok: true,
                    data: user
                })
            })
            .catch(err => {
                resolve({
                    ok: false,
                    err: err
                })

            })
    })
};

verifyUser = async (img) => {
    return new Promise((resolve, reject) => {
        axios.post("http://api-img-xjp.fengkongcloud.com/image/v4", {
            "accessKey": "3kyImf7P9QdquwX38QV9",
            "appId": "default",
            "type": "BAN_POLITICS_VIOLENCE_PORN",
            "eventId": "dynamic",
            "data": {
                "tokenId": "YNRpw5lPvYOLHQm9KhIR",
                "img": img
            }
        })
            .then(res => {
                resolve({
                    ok: true,
                    data: res.data
                })
            })
            .catch(err => {
                resolve({
                    ok: false,
                    err: err
                })
            })

    })
}

getUserAndVerify = async (id) => {
    return new Promise(async (resolve, reject) => {
        const user = await getUserById(id);
        if (!user.ok) {
            console.log("🚫 ~ Failed to get user : ", user.err)
            return
        }
        // console.log("✅ ~ User : ", user.data)

        const verifyRealPicture = await verifyUser(user.data.realPicture);
        if (!verifyRealPicture.ok) {
            console.log("🚫~ Failed verifyRealPicture : ", verifyRealPicture.err)
        }


        let returnableData = {
            id: id,
            name: user.data.nickName,
            'Real Picture': user.data.realPicture,
            'Real Picture Verification': `${verifyRealPicture.data.message} | ${verifyRealPicture.data.riskLevel} | ${verifyRealPicture.data.riskDescription}`,
        };


        // let albumsResults = []

        for (index in user.data.albumPictures) {
            let albumPicture = user.data.albumPictures[index]
            const verifyAlbumPicture = await verifyUser(albumPicture);
            if (!verifyAlbumPicture.ok) {
                console.log(`🚫~ Failed albumPicture for ${albumPicture}. Result : `, verifyAlbumPicture.err)
            }
            // albumsResults.push({
            //     albumPicture: albumPicture,
            //     verificationResults: {
            //         message: verifyAlbumPicture.data.message,
            //         riskLevel: verifyAlbumPicture.data.riskLevel,
            //     }
            // });

            returnableData[`Album Picture ${index}`] = albumPicture
            returnableData[`Album Picture ${index} Verification`] = `${verifyAlbumPicture.data.message} | ${verifyAlbumPicture.data.riskLevel} | ${verifyAlbumPicture.data.riskDescription}`
        }




        resolve(returnableData)
    })
}

writeToCSV = async (list, filePath) => {
    return new Promise(async (resolve, reject) => {
        const csv = new ObjectsToCsv(list)
        await csv.toDisk(filePath, { append: true })
        resolve()
    })
}

verifyAndWriteToCSV = async (id, filePath) => {
    return new Promise(async (resolve, reject) => {
        let result = await getUserAndVerify(id);
        await writeToCSV([result], filePath)
        resolve()
    })
}




getUserAndWriteToCSV = async (index, id, filePath) => {
    return new Promise(async (resolve, reject) => {

        const user = await getUserById(id);
        if (!user.ok) {
            // console.log(`🚫 ~ Failed to get User with ID = ${id} | Error :`, user.err.data)
            resolve({
                ok: false,
                err: user.err
            })
            return
        }

        let returnableData = {
            name: `User ${index}`,
            'Head Image': user.data.realPicture
        };


        user.data.albumPictures.forEach((item, i) => {
            returnableData[`Album Picture # ${i + 1}`] = item;
        })




        await writeToCSV([returnableData], filePath)

        resolve({
            ok: true
        });


    });
}

verifyFaceCompare = async (userId, headImage, albumPicture, upperIndex) => {
    return new Promise((resolve, reject) => {
        console.log(`--  💬  Sending face compare request `);
        let url = "http://api-img-sh.fengkongcloud.com/image/v4"
        axios.post(url,
            {
                "accessKey": "3kyImf7P9QdquwX38QV9",
                "type": "POLITICS_PORN_VIOLENCE_BAN",
                "appId": "default",
                "eventId": "headImage",
                "businessType": "FACECOMPARE",
                "data": {
                    "img": albumPicture,
                    "imgCompareBase": headImage,
                    "tokenId": "100212"
                }
            }

        )
            .then(r => {
                console.log(`--  ☑️   Face compare request finished.`);
                try {
                    let contents = JSON.parse(JSON.stringify(r.data));
                    let businessLabels = JSON.parse(JSON.stringify(contents.businessLabels))
                    let firstLabel = JSON.parse(JSON.stringify(businessLabels[0]))
                    resolve({
                        ok: true,
                        data: {
                            probability: firstLabel.probability,
                            confidenceLevel: firstLabel.confidenceLevel,
                            rawData: JSON.stringify(r.data)
                        }
                    })
                } catch (error) {
                    resolve({
                        ok: false,
                        data: {
                            rawData: JSON.stringify(r.data)
                        }
                    })
                }


                // setTimeout(() => {
                //     console.log(`--  💬  Sending request to fetch face compare result `);
                //     axios.post("http://api-img-bj.fengkongcloud.com/v4/image/query",
                //         {
                //             "accessKey": "3kyImf7P9QdquwX38QV9",
                //             "appId": "default",
                //             "requestIds": [
                //                 {
                //                     "requestId": res.data.requestIds[0].requestId,
                //                     "btId": "123123123"
                //                 }
                //             ]
                //         }
                //     )
                //         .then(r => {
                //             console.log(`--  ☑️   Face compare result fetch done.`);
                //             try {
                //                 let contents = JSON.parse(JSON.stringify(r.data.contents[0]));
                //                 let Result = JSON.parse(JSON.stringify(contents.Result))
                //                 let businessLabels = JSON.parse(JSON.stringify(Result.businessLabels))
                //                 let firstLabel = JSON.parse(JSON.stringify(businessLabels[0]))
                //                 resolve({
                //                     ok: true,
                //                     data: {
                //                         probability: firstLabel.probability,
                //                         confidenceLevel: firstLabel.confidenceLevel,
                //                         rawData: JSON.stringify(r.data)
                //                     }
                //                 })
                //             } catch (error) {
                //                 resolve({
                //                     ok: false,
                //                     data: {
                //                         rawData: JSON.stringify(r.data)
                //                     }
                //                 })
                //             }

                //         })

                // }, 20000);

            })
        // .catch(err => {
        //     resolve({
        //         ok: false,
        //         err: err
        //     })
        // })



    })
}

getUserAndDoFaceCompare = async (id, filePath) => {
    return new Promise(async (resolve, reject) => {
        console.log('--  💬  Sending user fetch request ');
        const user = await getUserById(id);

        if (!user.ok) {
            resolve({
                ok: false,
                err: user.err
            })
            return
        }
        console.log("--  ☑️   User fetch request request finished.");
        for (albumIndex in user.data.albumPictures) {
            console.log(`--  🔉  Initiating face compare (${Number(albumIndex) + 1}/${user.data.albumPictures.length})`);
            let res = await verifyFaceCompare(id, user.data.realPicture, user.data.albumPictures[albumIndex], Number(albumIndex))
            if (res.ok) {
                console.log(`--  🔉  Preparing to write data to CSV.`);
                await writeToCSV([{
                    ID: id,
                    "Head Image": user.data.realPicture,
                    "Album Picture": user.data.albumPictures[albumIndex],
                    "Probability": res.data.probability,
                    "Confidence Level": res.data.riskLevel,
                    "Raw Response": res.data.rawData
                }], filePath)
                console.log(`--  ☑️   Writing to CSV finished.`);
            } else {
                console.log(`--  🚫  Failed face compare (${Number(albumIndex) + 1}/${user.data.albumPictures.length}). Skipping to next`);
                await writeToCSV([{
                    ID: id,
                    "Head Image": user.data.realPicture,
                    "Album Picture": user.data.albumPictures[albumIndex],
                    "Probability": "--",
                    "Confidence Level": "--",
                    "Raw Response": res.data.rawData
                }], filePath)
            }
        }
        await writeToCSV([{
            ID: "",
            "Head Image": "",
            "Album Picture": "",
            probability: "",
            "Risk Level": ""
        }, {
            ID: "",
            "Head Image": "",
            "Album Picture": "",
            probability: "",
            "Risk Level": ""
        }], filePath)
        resolve({
            ok: true
        })
    })
}


(async () => {

    let setOne = [
        "b8b5a548-a616-590e-9be5-da3fb6a56b28",
        "895e869f-f4d3-52a7-bbc0-fa6d8849238d",
        "f43c0878-808e-5eaf-a3be-191603c6d8ec",
        "6aa0c040-9c10-5f43-b3f0-ffa1e3bd191d",
        "051adc5e-efe1-5070-a948-f58fe50cd603",
        "3961ba11-81fd-5f91-a31f-5af19240a0f0",
        "cf0d8e2b-fac2-56f7-8f31-1cba5313a2d9",
        "e33d1bb2-d607-5c05-a7f8-083bc0b4bb62",
        "24c4eb5d-0def-54f5-bd0b-d3600ff3aaee",
        "fbf917d9-e0b6-5ab2-a147-43b6fc6fa2ce",
        "e4b8b821-936c-5f80-af8b-b2ac1cc02eb2",
        "e2182fe0-b503-51b6-adf1-ac7f9abbc36b",
        "3a220ad3-0b8d-50d1-8658-c02f6b9e9658",
        "086850d0-384b-5c2c-9085-531aad2a2db0",
        "e6740a34-4863-50f6-81e5-eb8f8de57edc",
    ]


    let setTwo = [
        "471ce831-65fe-5e30-abc0-efa2c2d1760d",
        "e5c6f594-7f53-5203-a840-3942bca0ec6c",
        "f83c046b-4b35-5f50-8577-977653b40757",
        "ed797f0c-8ce7-50a9-a529-3e9df700a2be",
        "8f393aed-218c-5324-8cd8-ce8e332ff5c2",
        "2beecaf8-4ab0-5b5d-8333-bdf3637c3b84",
        "6b7d4d24-8079-5a26-921a-72b53c60d022",
        "6a36163d-8b3c-5055-9514-bf014cb98d5e",
        "c9e2f9a0-e3aa-53e2-97e6-b4ab808a8ee8",
        "b27e28c2-ea4e-5a09-b321-1aa591213f28",
        "2c2b7431-4c86-51d6-920b-8ce2018cc234",
        "69365ab7-83f1-5fd7-82b9-c0109142aefb",
        "a19ce0dd-4622-55d8-b457-622be6b8cfbf",
        "2fcdc17e-6e9e-598c-8276-6c30422b3d0f",
        "5482a81c-a6c4-54e5-ae98-b7e366e83a66",
        "b6802a3e-4016-51ff-9c3a-16340a810691",
        "f7791c96-fe19-5d23-816e-ed0d888097e8"
    ]

    let fullSet = [
        ...setOne,
        ...setTwo
    ];

    for (index in fullSet) {

        let num = Number(index) + 1;
        let id = fullSet[index];
        console.log(`🚀 ~ User(${num}/${fullSet.length}) - Starting fetch and verify for user with ID = ${id}`)
        let res = await getUserAndDoFaceCompare(id, './Probablity.csv')
        if (res.ok) {
            console.log(`✅  ~ User(${num}/${fullSet.length}) face compare test done and results saved in CSV.`);
            console.log(``);
        } else {
            console.log(`🚫  User (${num}/${fullSet.length}) with ID = ${id} failed. Error = `, res.err.message);
            console.log(``);
        }

    }


    // console.log("");
    // console.log("=========================================");
    // console.log("✅ ~ Verified User : ")
    // console.log("ID = ", result.id);
    // console.log("Name = ", result.name);
    // console.log('Real Picture : ', result.realPicture);
    // console.log('Album Pictures : ', result.albumPictures);
    // console.log("=========================================");

})().catch(e => {
    console.log("🚫~ file: index.js ~ line 27 ~ e", e)
    // Deal with the fact the chain failed
});

